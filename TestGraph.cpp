// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif



TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}


//------------
// add_vertex
//------------

TYPED_TEST(GraphFixture, av0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_FALSE(vdA == vdB);
}

TYPED_TEST(GraphFixture, av1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    for(int i = 0; i < 10; ++i) {
        ASSERT_TRUE(add_vertex(g) == (vertex_descriptor) i);
    }
}

//----------
// add_edge
//----------

TYPED_TEST(GraphFixture, ae0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e0 = add_edge(vdA, vdB, g);
    ASSERT_EQ(e0.second, true);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(e0.first,  e1.first);
    ASSERT_EQ(e1.second, false);
}

TYPED_TEST(GraphFixture, ae1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);


    pair<edge_descriptor, bool> e0 = add_edge(vdA, vdB, g);
    ASSERT_EQ(e0.second, true);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdC, g);
    ASSERT_NE(e1.first,  e0.first);
    ASSERT_EQ(e1.second, true);
}


//--------
// vertex
//--------

TYPED_TEST(GraphFixture, v0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);
}

TYPED_TEST(GraphFixture, v1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    vertex_descriptor vd0 = vertex(0, g);
    vertex_descriptor vd1 = vertex(1, g);
    ASSERT_EQ(vd0, vdA);
    ASSERT_EQ(vd1, vdB);
}

TYPED_TEST(GraphFixture, v2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd0 = vertex(0, g);
    vertex_descriptor vd1 = vertex(0, g);
    ASSERT_EQ(vd0, vdA);
    ASSERT_EQ(vd1, vdA);
    ASSERT_EQ(vd0, vd1);
}


//------
// edge
//------

TYPED_TEST(GraphFixture, e0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    ASSERT_TRUE(num_edges(g) == 2u);
    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdA, vdC, g);
    pair<edge_descriptor, bool> e3 = edge(vdB, vdC, g);

    ASSERT_EQ(edAB, e1.first);
    ASSERT_TRUE(e1.second);

    ASSERT_EQ(edAC, e2.first);
    ASSERT_TRUE(e2.second);

    ASSERT_FALSE(e3.second);
}

TYPED_TEST(GraphFixture, e1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    ASSERT_FALSE(e1.second);

    add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdA, vdB, g);
    ASSERT_TRUE(e2.second);
}

TYPED_TEST(GraphFixture, e2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdA, g);
    pair<edge_descriptor, bool> e2 = edge(vdB, vdB, g);

    ASSERT_TRUE(e1.second);
    ASSERT_TRUE(e2.second);

    pair<edge_descriptor, bool> e3 = edge(vdA, vdA, g);
    pair<edge_descriptor, bool> e4 = edge(vdB, vdB, g);

    ASSERT_EQ(e1.first, e3.first);
    ASSERT_EQ(e2.first, e4.first);
}

//--------------
// num_vertices
//--------------

TYPED_TEST(GraphFixture, nv0) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_TRUE(num_vertices(g) == 0u);
}

TYPED_TEST(GraphFixture, nv1) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    for(int i = 0; i < 100; ++i) {
        add_vertex(g);
    }
    ASSERT_TRUE(num_vertices(g) == 100u);
}


//-----------
// num_edges
//-----------

TYPED_TEST(GraphFixture, ne0) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_TRUE(num_edges(g) == 0u);
}

TYPED_TEST(GraphFixture, ne1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);

    ASSERT_TRUE(num_edges(g) == 3u);
}

TYPED_TEST(GraphFixture, ne2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e1  = add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    ASSERT_FALSE(e1.second);
    ASSERT_TRUE(num_edges(g) == 2u);
}

//-------
// source
//-------

TYPED_TEST(GraphFixture, s0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(e1.first, g);
    ASSERT_EQ(vd1, vdA);

    pair<edge_descriptor, bool> e2 = edge(vdA, vdC, g);
    vertex_descriptor vd2 = source(e2.first, g);
    ASSERT_EQ(vd2, vdA);
}

//-------
// target
//-------

TYPED_TEST(GraphFixture, t0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = target(e1.first, g);
    ASSERT_EQ(vd1, vdB);

    pair<edge_descriptor, bool> e2 = edge(vdA, vdC, g);
    vertex_descriptor vd2 = target(e2.first, g);
    ASSERT_EQ(vd2, vdC);
}

// ------------------
// adjacency iterator
// ------------------

TYPED_TEST(GraphFixture, ai0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> a1 = adjacent_vertices(vdA, g);
    adjacency_iterator                           b1 = a1.first;
    adjacency_iterator                           e1 = a1.second;
    ASSERT_TRUE(b1 == e1);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> a2 = adjacent_vertices(vdA, g);
    adjacency_iterator                           b2 = a2.first;
    adjacency_iterator                           e2 = a2.second;
    ASSERT_FALSE(b2 == e2);
}

TYPED_TEST(GraphFixture, ai1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdB, vdA, g);
    add_edge(vdB, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> a = adjacent_vertices(vdB, g);
    adjacency_iterator                           b = a.first;
    adjacency_iterator                           e = a.second;
    ASSERT_FALSE(b == e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);

    ++b;
    ASSERT_FALSE(b == e);
    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);

    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, ai2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for (int i = 0; i < 100; ++i) {
        add_vertex(g);
        add_edge(vdA, (vertex_descriptor) i, g);
    }

    pair<adjacency_iterator, adjacency_iterator> a = adjacent_vertices(vdA, g);
    int count = 0;
    adjacency_iterator b = a.first;
    adjacency_iterator e = a.second;
    while (b != e) {
        ++count;
        ++b;
    }

    ASSERT_EQ(num_edges(g), 100);
    ASSERT_EQ(count, 100);

}

// ---------------
// vertex iterator
// ---------------

TYPED_TEST(GraphFixture, vi0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> p1 = vertices(g);
    vertex_iterator                           b1 = p1.first;
    vertex_iterator                           e1 = p1.second;
    ASSERT_TRUE(b1 == e1);

    for (int i = 0; i < 100; ++i) {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p2 = vertices(g);
    vertex_iterator                           b2 = p2.first;
    vertex_iterator                           e2 = p2.second;
    ASSERT_FALSE(b2 == e2);
}

TYPED_TEST(GraphFixture, vi1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                      b = p.first;
    vertex_iterator                      e = p.second;
    ASSERT_FALSE(b == e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);

    ++b;
    ASSERT_FALSE(b == e);
    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);

    ++b;
    ASSERT_FALSE(b == e);
    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, vdC);

    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, vi2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for (int i = 0; i < 100; ++i) {
        add_vertex(g);
        add_edge(vdA, (vertex_descriptor) i, g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    int count = 0;
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    while (b != e) {
        ++count;
        ++b;
    }

    ASSERT_EQ(num_edges(g), 100);
    ASSERT_EQ(count, 101);
}

// -------------
// edge iterator
// -------------

TYPED_TEST(GraphFixture, ei0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator  = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_iterator, edge_iterator> p1 = edges(g);
    edge_iterator                           b1 = p1.first;
    edge_iterator                           e1 = p1.second;
    ASSERT_TRUE(b1 == e1);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<edge_iterator, edge_iterator> p2 = edges(g);
    edge_iterator                           b2 = p2.first;
    edge_iterator                           e2 = p2.second;
    ASSERT_FALSE(b2 == e2);
}

TYPED_TEST(GraphFixture, ei1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_FALSE(b == e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);

    ++b;
    ASSERT_FALSE(b == e);
    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);

    ++b;
    ASSERT_FALSE(b == e);
    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBC);

    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, ei2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator  = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for (int i = 0; i < 100; ++i) {
        vertex_descriptor vdI = add_vertex(g);
        add_edge(vdI, vdA, g);
    }

    pair<edge_iterator, edge_iterator> p = edges(g);
    int count = 0;
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    while (b != e) {
        ++count;
        ++b;
    }

    ASSERT_EQ(num_edges(g), 100);
    ASSERT_EQ(count, 100);

}

