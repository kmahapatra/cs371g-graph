// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph {
public:
    friend class vertex_iterator;
    friend class edge_iterator;

    // ------
    // usings
    // ------

    using vertex_descriptor = int;
    using edge_descriptor   = std::pair<int, int>;

    using vertices_size_type = std::size_t;
    using edges_size_type    = std::size_t;

    // ------
    // vertex_iterator
    // ------
    class vertex_iterator {
        friend class Graph;

        friend bool operator == (const vertex_iterator& lhs, const vertex_iterator& rhs) {
            return lhs.index_ == rhs.index_;
        }

        friend bool operator != (const vertex_iterator& lhs, const vertex_iterator& rhs) {
            return !(lhs == rhs);
        }

    public:
        // ------
        // usings
        // ------

        using iterator_category = std::bidirectional_iterator_tag;
        using pointer           = typename Graph::vertex_descriptor*;
        using reference         = typename Graph::vertex_descriptor;
        using size_type = typename Graph::vertices_size_type;
        using difference_type = ptrdiff_t;


    private:
        // ----
        // data
        // ----

        vertices_size_type index_ = 0;

    public:

        vertex_iterator(vertices_size_type index) : index_(index) {}

        reference operator * () const {
            return index_;
        }

        vertex_iterator& operator ++ () {
            ++index_;
            return *this;
        }

        vertex_iterator operator ++ (int) {
            vertex_iterator x = *this;
            ++(*this);
            return x;
        }

        vertex_iterator& operator -- () {
            --index_;
            return *this;
        }

        vertex_iterator operator -- (int) {
            vertex_iterator x = *this;
            --(*this);
            return x;
        }
    };

    // ------
    // edge_iterator
    // ------

    class edge_iterator {
        friend class Graph;

        friend bool operator == (const edge_iterator& lhs, const edge_iterator& rhs) {
            return (lhs.vertex_index_ == rhs.vertex_index_) && (lhs.edge_index_ == rhs.edge_index_);
        }

        friend bool operator != (const edge_iterator& lhs, const edge_iterator& rhs) {
            return !(lhs == rhs);
        }

    public:
        // ------
        // usings
        // ------

        using iterator_category = std::forward_iterator_tag;
        using pointer           = typename Graph::edge_descriptor*;
        using reference         = typename Graph::edge_descriptor;
        using vertices_size_type = typename Graph::vertices_size_type;
        using edges_size_type = typename Graph::edges_size_type;
        using difference_type = ptrdiff_t;

    private:
        // ----
        // data
        // ----


        vertices_size_type vertex_index_ = 0;
        edges_size_type edge_index_ = 0;
        const Graph* g_ = nullptr;

    public:

        edge_iterator(vertices_size_type vertex_index, edges_size_type edge_index, const Graph* g) :
            vertex_index_(vertex_index), edge_index_(edge_index), g_(g) {}

        reference operator * () const {
            return std::make_pair(vertex_index_, g_->vertices_[vertex_index_][edge_index_]);
        }

        edge_iterator& operator ++ () {
            while (++edge_index_ >= g_->vertices_[vertex_index_].size()) {
                edge_index_ = 0;
                if (++vertex_index_ == g_->vertices_.size() ||
                        g_->vertices_[vertex_index_].size() > 0) {
                    break;
                }
            }
            return *this;
        }

        edge_iterator operator ++ (int) {
            edge_iterator x = *this;
            ++(*this);
            return x;
        }
    };

    // ------
    // adjacency_iterator
    // ------

    class adjacency_iterator {
        friend class Graph;

        friend bool operator == (const adjacency_iterator& lhs, const adjacency_iterator& rhs) {
            return (lhs.vertex_index_ == rhs.vertex_index_) && (lhs.edge_index_ == rhs.edge_index_);
        }

        friend bool operator != (const adjacency_iterator& lhs, const adjacency_iterator& rhs) {
            return !(lhs == rhs);
        }

    public:
        // ------
        // usings
        // ------

        using iterator_category = std::bidirectional_iterator_tag;
        using pointer           = typename Graph::vertex_descriptor*;
        using reference         = typename Graph::vertex_descriptor;
        using vertices_size_type = typename Graph::vertices_size_type;
        using edges_size_type = typename Graph::edges_size_type;
        using difference_type = ptrdiff_t;

    private:
        // ----
        // data
        // ----


        vertices_size_type vertex_index_ = 0;
        edges_size_type edge_index_ = 0;
        const Graph* g_ = nullptr;

    public:

        adjacency_iterator(vertices_size_type vertex_index, edges_size_type edge_index, const Graph* g) :
            vertex_index_(vertex_index), edge_index_(edge_index), g_(g) {}

        reference operator * () const {
            return g_->vertices_[vertex_index_][edge_index_];
        }

        adjacency_iterator& operator ++ () {
            ++edge_index_;
            return *this;
        }

        adjacency_iterator operator ++ (int) {
            adjacency_iterator x = *this;
            ++(*this);
            return x;
        }

        adjacency_iterator& operator -- () {
            --edge_index_;
            return *this;
        }

        adjacency_iterator operator -- (int) {
            adjacency_iterator x = *this;
            --(*this);
            return x;
        }

    };


public:
// --------
// add_edge
// --------

    /**
     * Adds edge (v1, v2) to graph, returns edge descriptor for new edge
     */
    friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor v1, vertex_descriptor v2, Graph& g) {
        bool b = false;
        auto& adjs = g.vertices_[v1];
        if (std::find(adjs.begin(), adjs.end(), v2) == adjs.end()) {
            adjs.push_back(v2);
            b = true;
        }
        edge_descriptor ed = std::make_pair(v1, v2);
        return std::make_pair(ed, b);
    }

// ----------
// add_vertex
// ----------

    /**
     * Adds vertex to graph, returns vertex descriptor for new vertex
     */
    friend vertex_descriptor add_vertex (Graph& g) {
        std::vector<vertex_descriptor> adjs;
        g.vertices_.push_back(adjs);
        return num_vertices(g) - 1;
    }

// -----------------
// adjacent_vertices
// -----------------

    /**
     * Returns an iterator-range across vertices adjacent to vertex v in graph g
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor v, const Graph& g) {
        adjacency_iterator b = adjacency_iterator(v, 0, &g);
        adjacency_iterator e = adjacency_iterator(v, g.vertices_[v].size(), &g);
        return std::make_pair(b, e);
    }

// ----
// edge
// ----

    /**
     * Return a pair containing an edge descriptor from vertex v1 to vertex v2 and if one such edge exists in the graph
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor v1, vertex_descriptor v2, const Graph& g) {
        const auto& adjs = g.vertices_[v1];
        bool b = std::find(adjs.begin(), adjs.end(), v2) != adjs.end();
        edge_descriptor ed = std::make_pair(v1, v2);
        return std::make_pair(ed, b);
    }

// -----
// edges
// -----

    /**
     * Returns an iterator-range across edge set of graph g
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
        const int vertex_count = g.vertices_.size();
        for (vertex_descriptor v = 0; v < vertex_count; ++v) {
            if (g.vertices_[v].size() == 0) {
                continue;
            }
            edge_iterator b = edge_iterator(v, 0, &g);
            return std::make_pair(b, edge_iterator(vertex_count, 0, &g));
        }
        edge_iterator b = edge_iterator(0, 0, &g);
        return std::make_pair(b, b);
    }

// ---------
// num_edges
// ---------

    /**
     * Returns number of edges in graph g
     */
    friend edges_size_type num_edges (const Graph& g) {
        edges_size_type s = 0;
        for (const auto& adjs : g.vertices_) {
            s += adjs.size();
        }
        return s;
    }

// ------------
// num_vertices
// ------------

    /**
     * Returns number of vertices in graph g
     */
    friend vertices_size_type num_vertices (const Graph& g) {
        return g.vertices_.size();
    }

// ------
// source
// ------

    /**
     * Returns source vertex of edge e
     */
    friend vertex_descriptor source (edge_descriptor e, const Graph&) {
        return e.first;
    }

// ------
// target
// ------

    /**
     * Returns target vertex of edge e
     */
    friend vertex_descriptor target (edge_descriptor e, const Graph&) {
        return e.second;
    }

// ------
// vertex
// ------

    /**
     * Returns nth vertex in graph's vertex list
     */
    friend vertex_descriptor vertex (vertices_size_type n, const Graph& g) {
        assert(n < g.vertices_.size());
        return n;
    }

// --------
// vertices
// --------

    /**
     * Returns an iterator-range across the vertex set of graph g
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
        vertex_iterator b = vertex_iterator(0);
        vertex_iterator e = vertex_iterator(num_vertices(g));

        return std::make_pair(b, e);
    }


private:
// ----
// data
// ----

    std::vector<std::vector<vertex_descriptor>> vertices_;

// -----
// valid
// -----

    /**
     * Check if all the edges refer to valid vertices
     */
    bool valid () const {
        const int vertex_count = vertices_.size();
        for (const auto& v : vertices_) {
            for (vertex_descriptor i : v) {
                if (i < 0 || i >= vertex_count) {
                    return false;
                }
            }
        }
        return true;
    }

public:
// --------
// defaults
// --------

    Graph             ()             = default;
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;

};




#endif // Graph_h
